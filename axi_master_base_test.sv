

`include "axi_model_env.sv"
class axi_master_base_test extends uvm_test;

  `uvm_component_utils(axi_master_base_test)
  
  
  axi_model_env env;

  
  function new(string name = "axi_master_base_test",uvm_component parent=null);
    super.new(name,parent);
  endfunction : new

  
  virtual function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    env = axi_model_env::type_id::create("env", this);
  endfunction : build_phase
  
    
  virtual function void end_of_elaboration();
    print();
  endfunction

  

endclass:axi_master_base_test
