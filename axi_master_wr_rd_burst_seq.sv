

class axi_master_wr_rd_burst_seq extends uvm_sequence#(axi_seq_item);
  
  `uvm_object_utils(axi_master_wr_rd_burst_seq)
 
  function new(string name = "axi_master_wr_rd_burst_seq");
    super.new(name);
  endfunction
  
  bit [31:0] raddr;
  bit [3:0] length;
  bit [31:0] waddr;
  
  virtual task body();
    wait_for_grant();
    req.randomize();
    
    req.write_start_i     = 1'b1;
    req.write_awid_i      = 4'b0001;
    req.write_addr_i      = this.waddr;
    req.write_length_i    = this.length;
    req.write_size_i      = 3'b100;
    req.write_burst_i     = 2'b00;
    req.write_lock_i      = 2'b00;
    req.write_wid_i       = 4'b0001;                     
    req.write_datav_i     = 1'b1;
    req.write_strb_i      = 4'b1111;
    
    send_request(req);
    wait_for_item_done();
    
    
    req = axi_seq_item::type_id::create("req");
    wait_for_grant();
    req.randomize();
    
    req.read_start_i     = 1'b1;
    req.read_arid_i      = 4'b0001;
    req.read_addr_i      = this.raddr;
    req.read_length_i    = this.length;
    req.read_size_i      = 3'b100;
    req.read_burst_i     = 2'b00;
    req.read_lock_i      = 2'b00;
    req.read_af_i        = 1'b0;                      
                        
    send_request(req);
    wait_for_item_done();
    
  endtask : body
  
endclass : axi_master_wr_rd_burst_seq
