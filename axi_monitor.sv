

class axi_monitor extends uvm_monitor;

  virtual axi_if vif;
  
  bit [3:0] count = 4'b0000;

  
  uvm_analysis_port #(axi_seq_item) item_collected_port;
  
  axi_seq_item trans_collected;

  `uvm_component_utils(axi_monitor)

  function new (string name, uvm_component parent);
    super.new(name, parent);
	
    item_collected_port = new("item_collected_port", this);
  endfunction : new

  function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    if(!uvm_config_db#(virtual axi_if)::get(this, "", "vif", vif))
       `uvm_fatal("NOVIF",{"virtual interface must be set for: ",get_full_name(),".vif"});
  endfunction: build_phase
  
  virtual task run_phase(uvm_phase phase);
    forever begin
      trans_collected = new();
       
      @(posedge vif.aclk_i);
      wait(vif.write_start_i)begin 
          trans_collected.write_start_i   = vif.write_start_i;
          trans_collected.write_addr_i    = vif.write_addr_i;
          trans_collected.write_size_i    = vif.write_size_i;
          trans_collected.write_length_i  = vif.write_length_i;
          trans_collected.write_burst_i   = vif.write_burst_i;
          trans_collected.write_lock_i    = vif.write_lock_i;
          trans_collected.write_awid_i    = vif.write_awid_i;
          trans_collected.write_wid_i     = vif.write_wid_i;
          trans_collected.write_data_i    = vif.write_data_i;
          trans_collected.write_datav_i   = vif.write_datav_i;
          trans_collected.write_strb_i    = vif.write_strb_i;
          
           
                    
          if(vif.write_length_i==0) begin
          wait(vif.write_ack_o);
          @(posedge vif.aclk_i);
          trans_collected.write_start_i   = vif.write_start_i;
                  
          
          wait(vif.write_done_o);
          @(posedge vif.aclk_i);
          trans_collected.write_ack_o        = vif.write_ack_o;
          trans_collected.write_data_req_o   = vif.write_data_req_o;
          trans_collected.write_done_o       = vif.write_done_o;
          trans_collected.write_err_o        = vif.write_err_o;
          trans_collected.write_bresp_o      = vif.write_bresp_o;
          trans_collected.write_bid_o        = vif.write_bid_o;
          trans_collected.write_bvalid_o     = vif.write_bvalid_o;
          
                            
        end
        else begin 
          wait(vif.write_ack_o);
          @(posedge vif.aclk_i);
          trans_collected.write_start_i   = vif.write_start_i;
         
          for(int i =0;i<=(vif.write_length_i) ;i++) begin    
            @(posedge vif.aclk_i);
            if(vif.write_data_req_o) begin
              trans_collected.write_data_i= vif.write_data_i;
              count = count+4'b0001;
             
              trans_collected.write_data_i    = vif.write_data_i;
           
             
              end
               
            end
          end
        
          wait(vif.write_done_o);
          trans_collected.write_ack_o        = vif.write_ack_o;
          trans_collected.write_data_req_o   = vif.write_data_req_o;
          trans_collected.write_done_o       = vif.write_done_o;
          trans_collected.write_err_o        = vif.write_err_o;
          trans_collected.write_bresp_o      = vif.write_bresp_o;
          trans_collected.write_bid_o        = vif.write_bid_o;
          trans_collected.write_bvalid_o     = vif.write_bvalid_o;
        
        end
      
      wait(vif.read_start_i) begin
          
          trans_collected.read_arid_i     =   vif.read_arid_i;  
          trans_collected.read_addr_i     =   vif.read_addr_i;
          trans_collected.read_length_i   =   vif.read_length_i;
          trans_collected.read_size_i     =   vif.read_size_i ;
          trans_collected.read_burst_i    =   vif.read_burst_i; 
          trans_collected.read_lock_i     =   vif.read_lock_i ;  
          trans_collected.read_af_i       =   vif.read_af_i   ;
          
         
          wait(vif.read_ack_o); 	
          trans_collected.read_ack_o      =  vif.read_ack_o;
         
      
      wait(vif.read_datav_o);  
      if(vif.read_datav_o) begin
        trans_collected.read_data_o     =  vif.read_data_o;
        trans_collected.read_datav_o    =  vif.read_datav_o;
        trans_collected.read_err_o      =  vif.read_err_o;
        trans_collected.read_done_o     =  vif.read_done_o;
        
        @(posedge vif.aclk_i);
        
      end
      end
      item_collected_port.write(trans_collected);
    end
    
  endtask : run_phase

endclass : axi_monitor
