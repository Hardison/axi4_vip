
`include "axi_agent.sv"
`include "axi_scoreboard.sv"

class axi_model_env extends uvm_env;
  
  
  axi_agent      axi_agnt;
  axi_scoreboard axi_scb;
  
  `uvm_component_utils(axi_model_env)
  
  
  function new(string name, uvm_component parent);
    super.new(name, parent);
  endfunction : new

  
  function void build_phase(uvm_phase phase);
    super.build_phase(phase);

    axi_agnt = axi_agent::type_id::create("axi_agnt", this);
    axi_scb  = axi_scoreboard::type_id::create("axi_scb", this);
  endfunction : build_phase
  
  
  function void connect_phase(uvm_phase phase);
    axi_agnt.monitor.item_collected_port.connect(axi_scb.item_collected_export);
  endfunction : connect_phase

endclass : axi_model_env