
`ifndef tbtop 

`define tbtop
import uvm_pkg::* ;
`include "uvm_macros.svh"

`include "axi_master.sv"
`include "axi_interface.sv"
`include "axi_master_base_test.sv"

`include "axi_master_write_test.sv"
`include "axi_master_read_test.sv"
`include "axi_master_wr_rd_test.sv"
`include "axi_master_wr_burst_test.sv"
`include "axi_master_rd_burst_test.sv"
`include "axi_master_wr_rd_burst_test.sv"
`include "axi_master_m_write_test.sv"
`include "axi_master_m_read_test.sv"
`include "axi_master_m_wr_rd_test.sv"
`include "axi_master_m_wr_burst_test.sv"
`include "axi_master_m_rd_burst_test.sv"
`include "axi_master_m_wr_rd_burst_test.sv"

module axi_master_tb_top;

  
  bit aclk_i;
  bit areset_n_i;
  
  always #5 aclk_i = ~aclk_i;
  
  initial begin
    areset_n_i = 0;
    #5 areset_n_i =1;
  end
  
  axi_if intf(aclk_i,areset_n_i);
  
  
  wire [3:0]    awid_o;
  wire [31:0]   awaddr_o;
  wire [3:0]    awlen_o;
  wire [2:0]    awsize_o;
  wire [1:0]    awburst_o;
  wire [1:0]    awlock_o;
  wire          awvalid_o;
  wire           awready_i;
  
  wire [3:0]    wid_o;
  wire [127:0]  wdata_o;
  wire [15:0]   wstrb_o;
  wire wlast_o;
  wire wvalid_o;
  wire wready_i;
  
  wire [3:0]    bid_i;
  wire [1:0]    bresp_i;
  wire bvalid_i;
  wire bready_o;
  
  wire [3:0]    arid_o;
  wire [31:0]   araddr_o;
  wire [3:0]    arlen_o;
  wire [2:0]    arsize_o;
  wire [1:0]    arburst_o;
  wire [1:0]    arlock_o;
  wire arvalid_o;
  wire arready_i;
 
  wire [3:0]    rid_i;
  wire [127:0]  rdata_i;
  wire [1:0]    rresp_i;
  wire rlast_i;
  wire rvalid_i;
  wire rready_o;
  
  wire[2:0]     awprot_i;
  
  axi_master axi_master_i (
    
    .aclk_i        (aclk_i) ,      
    .areset_n_i    (areset_n_i) ,  
    
    .awid_o        (awid_o),                          
    .awaddr_o      (awaddr_o),
    .awlen_o       (awlen_o),
    .awsize_o      (awsize_o),
    .awburst_o     (awburst_o),
    .awlock_o      (awlock_o),
    .awvalid_o     (awvalid_o),
    .awready_i     (awready_i),
    
    .wid_o         (wid_o),
    .wdata_o       (wdata_o),
    .wstrb_o       (wstrb_o),
    .wlast_o 	   (wlast_o),
    .wvalid_o      (wvalid_o),
    .wready_i      (wready_i),
    
    .bid_i         (bid_i),
    .bresp_i       (bresp_i),
    .bvalid_i      (bvalid_i),
    .bready_o      (bready_o),
   
    .arid_o        (arid_o),
    .araddr_o      (araddr_o),
    .arlen_o       (arlen_o),
    .arsize_o      (arsize_o),
    .arburst_o     (arburst_o),
    .arlock_o      (arlock_o),
    .arvalid_o     (arvalid_o),
    .arready_i     (arready_i),
    
    .rid_i         (rid_i),
    .rdata_i       (rdata_i),
    .rresp_i       (rresp_i),
    .rlast_i       (rlast_i),
    .rvalid_i      (rvalid_i),
    .rready_o      (rready_o),   
    
   
    .write_start_i  (intf.write_start_i),
    .write_awid_i   (intf.write_awid_i),
    .write_addr_i   (intf.write_addr_i),
    .write_length_i (intf.write_length_i),
    .write_size_i   (intf.write_size_i),
    .write_burst_i  (intf.write_burst_i),
    .write_lock_i   (intf.write_lock_i),
    .write_wid_i    (intf.write_wid_i),
    .write_data_i   (intf.write_data_i),
    .write_datav_i  (intf.write_datav_i),
    .write_strb_i   (intf.write_strb_i),
    .write_ack_o    (intf.write_ack_o),
    .write_data_req_o(intf.write_data_req_o),
    .write_done_o    (intf.write_done_o),
    .write_err_o     (intf.write_err_o),
    .write_bresp_o   (intf.write_bresp_o),
    .write_bid_o     (intf.write_bid_o),
    .write_bvalid_o  (intf.write_bvalid_o),
    
    .read_start_i   (intf.read_start_i),
    .read_arid_i    (intf.read_arid_i),
    .read_addr_i    (intf.read_addr_i),
    .read_length_i  (intf.read_length_i),
    .read_size_i    (intf.read_size_i),
    .read_burst_i   (intf.read_burst_i),
    .read_lock_i    (intf.read_lock_i),
    .read_af_i      (intf.read_af_i),
    .read_ack_o     (intf.read_ack_o),
    .read_data_o    (intf.read_data_o),
    .read_datav_o   (intf.read_datav_o),
    .read_err_o     (intf.read_err_o),
    .read_done_o    (intf.read_done_o));
 
  
  wire [3:0] awcache_i;
  
  axi_slave_top DUT ( .aclk_i    (aclk_i),
                     .areset_n_i (areset_n_i),
                     
                     .awid_i    (awid_o),
                     .awaddr_i  (awaddr_o),
                     .awlen_i   (awlen_o),
                     .awsize_i  (awsize_o),
                     .awburst_i (awburst_o),
                     .awvalid_i (awvalid_o),
                     .awlock_i  (awlock_o),
                     .awcache_i (awcache_i),
                     .awprot_i  (awprot_i),
                     .awready_o (awready_i),
                     
                     .wid_i     (wid_o),
                     .wdata_i   (wdata_o),
                     .wstrb_i   (wstrb_o),
                     .wlast_i   (wlast_o),
                     .wvalid_i  (wvalid_o),
                     .wready_o  (wready_i),
                     
                     .bid_o     (bid_i),
                     .bresp_o   (bresp_i),
                     .bvalid_o  (bvalid_i),
                     .bready_i  (bready_o),
                     
                     .arid_i     (arid_o  ),
                     .araddr_i   (araddr_o ),
                     .arlen_i    (arlen_o  ),
                     .arsize_i   (arsize_o ),
                     .arburst_i  (arburst_o),
                     .arvalid_i  (arvalid_o),
                     .arready_o  (arready_i),
                     
                     .rid_o      (rid_i),
                     .rdata_o    (rdata_i),
                     .rresp_o    (rresp_i),
                     .rlast_o    (rlast_i),
                     .rvalid_o   (rvalid_i),
                     .rready_i   (rready_o));
  
  
  initial begin 
    uvm_config_db#(virtual axi_if)::set(uvm_root::get(),"*","vif",intf);
    
  end
 
  initial begin 
    
    run_test("axi_master_m_wr_rd_test");
    
    #100000;
    $finish;
  end
  
endmodule
`endif