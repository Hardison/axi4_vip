
class axi_master_m_wr_rd_test extends axi_master_base_test;
  
  `uvm_component_utils(axi_master_m_wr_rd_test)
  
  
  
  axi_master_write_seq wseq;
  axi_master_read_seq rseq;
  int data_r = 5;   

  
  function new(string name = "axi_master_m_wr_rd_test",uvm_component parent=null);
    super.new(name,parent);
  endfunction : new

  
  virtual function void build_phase(uvm_phase phase);
    super.build_phase(phase);

    
    
    wseq = axi_master_write_seq::type_id::create("wseq");
    rseq = axi_master_read_seq::type_id::create("rseq");
    
  endfunction : build_phase
  
  
  task run_phase(uvm_phase phase);
    
    phase.raise_objection(this);
        
    wseq.waddr   = 32'h0;
    wseq.length  = 4'b0000;
    wseq.awid    = 4'b0001;
    wseq.wid     = 4'b0001;
    wseq.wdata   = 128'b1;
    
    rseq.raddr   = 32'h0;
    rseq.arid    = 4'b0001;
    rseq.length  = 4'b0000;    
   
           
    repeat (data_r) begin
      wseq.waddr   = wseq.waddr + 4;
      rseq.raddr   = wseq.waddr;      
      wseq.awid    = wseq.awid +1;
      wseq.wid     = wseq.wid +1;
      wseq.wdata   = wseq.wdata +1;
      rseq.arid    = rseq.arid +1;
      wseq.start(env.axi_agnt.sequencer);
      rseq.start(env.axi_agnt.sequencer);
    end
		
		
		#1000;
    
    phase.drop_objection(this);
    
    
    phase.phase_done.set_drain_time(this, 500);
  endtask : run_phase

endclass : axi_master_m_wr_rd_test