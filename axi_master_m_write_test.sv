
class axi_master_m_write_test extends axi_master_base_test;

  `uvm_component_utils(axi_master_m_write_test)
  
  
    
  axi_master_write_seq wseq;
  int data_r = 5;   
	
  function new(string name = "axi_master_m_write_test",uvm_component parent=null);
    super.new(name,parent);
  endfunction : new

  
  virtual function void build_phase(uvm_phase phase);
    super.build_phase(phase);

    
    wseq = axi_master_write_seq::type_id::create("wseq");
  endfunction : build_phase
  
  
  task run_phase(uvm_phase phase);
    
    phase.raise_objection(this);
    
    wseq.waddr   = 32'h0;
    wseq.length  = 4'b0000;
    wseq.awid    = 4'b0001;
    wseq.wid     = 4'b0001;
    wseq.wdata   = 128'b1;
    
    for(int i=0;i<data_r;i++) begin      
      wseq.waddr   = wseq.waddr + 4;
      wseq.awid    = 4'b0001+i;
      wseq.wid     = 4'b0001+i;  
      wseq.wdata   = 128'b1+i; 
      wseq.start(env.axi_agnt.sequencer);      
    end
    
    
    phase.drop_objection(this);
    
    
    phase.phase_done.set_drain_time(this, 500);
    
  endtask : run_phase
  
endclass : axi_master_m_write_test