
interface axi_if(input logic aclk_i,areset_n_i);
  
  
  logic           write_start_i;
  logic [3:0]     write_awid_i;
  logic [31:0]    write_addr_i;
  logic [3:0]     write_length_i;
  logic [2:0]     write_size_i;
  logic [1:0]     write_burst_i;
  logic [1:0]     write_lock_i;
  logic [3:0]     write_wid_i;
  logic [127:0]   write_data_i;
  logic           write_datav_i;
  logic [15:0]    write_strb_i;
  logic           write_ack_o;
  logic           write_data_req_o;
  logic           write_done_o;
  logic           write_err_o;
  logic  [1:0]    write_bresp_o;
  logic  [3:0]    write_bid_o;
  logic           write_bvalid_o;
  
  logic           read_start_i;
  logic [3:0]     read_arid_i;
  logic [31:0]    read_addr_i;
  logic [3:0]     read_length_i;
  logic [2:0]     read_size_i;
  logic [1:0]     read_burst_i;
  logic [1:0]     read_lock_i;
  logic           read_af_i;
  logic           read_ack_o;
  logic  [127:0]  read_data_o;
  logic           read_datav_o;
  logic           read_err_o;
  logic           read_done_o;
  
    
  
  

endinterface