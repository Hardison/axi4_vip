

class axi_master_wr_rd_seq extends uvm_sequence#(axi_seq_item);
  
  `uvm_object_utils(axi_master_wr_rd_seq)
   
  function new(string name = "axi_master_wr_rd_seq");
    super.new(name);
  endfunction
  
  bit [31:0] waddr;
  bit [3:0] length;
  bit [3:0] awid;
  bit [3:0] wid;
  bit [128:0] wdata;
  
  
  bit [31:0] raddr;
  bit [3:0] arid;
  
    
  virtual task body();
    
    req = axi_seq_item::type_id::create("req");
    wait_for_grant();
    req.randomize();
    
    req.write_start_i     = 1'b1;
    
    req.write_awid_i      = this.awid;
    req.write_addr_i      = this.waddr;
    req.write_length_i    = this.length;
    req.write_size_i      = 3'b100;
    req.write_burst_i     = 2'b00;
    req.write_lock_i      = 2'b00;
      
    req.write_wid_i       = 4'b0001;
    req.write_data_i     = this.wdata;
    req.write_datav_i     = 1'b1;
    req.write_strb_i      = 4'b1111;
    
    send_request(req);
    wait_for_item_done();
    
    
    req = axi_seq_item::type_id::create("req");
    wait_for_grant();
    req.randomize();
    
    req.read_start_i     = 1'b1;
    
    req.read_arid_i      = this.arid;
    req.read_addr_i      = this.raddr;
    req.read_length_i    = this.length;
    req.read_size_i      = 3'b100;
    req.read_burst_i     = 2'b00;
    req.read_lock_i      = 2'b00;
    req.read_af_i        = 1'b0;                      
                        
    send_request(req);
    wait_for_item_done();
    
  endtask : body
  
endclass :axi_master_wr_rd_seq
