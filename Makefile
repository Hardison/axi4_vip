UVM12 = +incdir+../uvm-1.2/src -l uvm_1_2
RTL_FILES_VERILOG = -f ./rtl.f
#RTL_FILES_VHDL = -f ./rtlvhd.f
TB = -f ./tb.f
#LIB_FILES = -f ./lib.f
XTORS_DIR = .



comp:
	vlib work
	
	vlog -work work -v95 -v2k -v2k5 -allow_duplicated_units +notimingchecks -timescale 1ps/1ps \
	$(RTL_FILES_VERILOG) | tee compver.log
	vlog ${UVM12} +incdir+$(XTORS_DIR)/Example_tb -f $(XTORS_DIR)/uvm.f | tee uvm_comp.log
	
rv_sim:
	vsim +access +relax +r -L unisim -interceptcoutput +notimingchecks -t 1ps \
		
	 -l simulation.log \
	 -i 5000000 +notimingchecks \
	 +UVM_TESTNAME=test_simple \
	 +UVM_NO_RELNOTES \
	 +UVM_VERBOSITY=UVM_MEDIUM
	


clean:
	rm -rf work dataset.asdb fcover.acdb simulation.log