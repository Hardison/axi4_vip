
class axi_master_m_read_test extends axi_master_base_test;

  `uvm_component_utils(axi_master_m_read_test)
  
  
  axi_master_read_seq rseq;
  int data_r = 5;    

 
   
  function new(string name = "axi_master_m_read_test",uvm_component parent=null);
    super.new(name,parent);
  endfunction : new

  
  virtual function void build_phase(uvm_phase phase);
    super.build_phase(phase);

    
    rseq = axi_master_read_seq::type_id::create("rseq");
  endfunction : build_phase
  
  
    task run_phase(uvm_phase phase);
    
    phase.raise_objection(this);
    rseq.raddr     = 32'h0;
    rseq.length    = 4'b0000;
    rseq.arid      = 4'b0001;
    
    for(int i=0;i<data_r;i++) begin
      rseq.raddr = rseq.raddr + 4;
      rseq.arid  = 4'b0001 + i; 
      rseq.start(env.axi_agnt.sequencer);
    end
    
    phase.drop_objection(this);
    
    
    phase.phase_done.set_drain_time(this, 500);
      
  endtask : run_phase

endclass : axi_master_m_read_test