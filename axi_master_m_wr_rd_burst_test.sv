
class axi_master_m_wr_rd_burst_test extends axi_master_base_test;
  
  `uvm_component_utils(axi_master_m_wr_rd_burst_test)
  
  
   
  axi_master_wr_burst_seq wbseq;
  axi_master_rd_burst_seq rbseq;
  int data_r = 5;   

  
  
  function new(string name = "axi_master_m_wr_rd_burst_test",uvm_component parent=null);
    super.new(name,parent);
  endfunction : new

  
  virtual function void build_phase(uvm_phase phase);
    super.build_phase(phase);

      
    wbseq = axi_master_wr_burst_seq::type_id::create("wbseq");
    rbseq = axi_master_rd_burst_seq::type_id::create("rbseq");
    
  endfunction : build_phase
  
  
  task run_phase(uvm_phase phase);
    phase.raise_objection(this);
        
    wbseq.waddr   = 32'h0;
    wbseq.length  = 4'b0101;
    wbseq.awid    = 4'b0001;
    wbseq.wid     = 4'b0001;
    wbseq.wdata    = 128'b1;
    
    rbseq.raddr   = 32'h0;
    rbseq.arid    = 4'b0001;
    rbseq.length  = 4'b0101;    
    
   
	
		
		
		repeat (data_r) begin
		
      wbseq.waddr   = wbseq.waddr + 4;
      rbseq.raddr   = wbseq.waddr;      
      wbseq.awid    = wbseq.awid +1;
      wbseq.wid     = wbseq.wid +1;
      wbseq.wdata   = wbseq.wdata+1;
      rbseq.arid    = rbseq.arid +1;
      wbseq.start(env.axi_agnt.sequencer);
      rbseq.start(env.axi_agnt.sequencer);
    end
		
            
    phase.drop_objection(this);
    
    
    phase.phase_done.set_drain_time(this, 1000);
   
  endtask : run_phase

endclass : axi_master_m_wr_rd_burst_test