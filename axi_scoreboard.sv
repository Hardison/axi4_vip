class axi_scoreboard extends uvm_scoreboard;
  
  axi_seq_item pkt_qu[$];
  
  bit   wready = 1;
  bit [127:0] slave_data_out;
  
  
  uvm_analysis_imp#(axi_seq_item, axi_scoreboard) item_collected_export;
  `uvm_component_utils(axi_scoreboard)

  function new (string name, uvm_component parent);
    super.new(name, parent);
  endfunction : new
 
 
  function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    item_collected_export = new("item_collected_export", this);
  endfunction: build_phase
  
  
  virtual function void write(axi_seq_item pkt);
    
    pkt_qu.push_back(pkt);
  endfunction : write

  virtual task run_phase(uvm_phase phase);
    axi_seq_item axi_pkt;
	
    forever begin
      wait(pkt_qu.size() > 0);
      axi_pkt = pkt_qu.pop_front();
	  
      
      if((axi_pkt.write_datav_i == 1'b1) && (wready == 1'b1)) begin
        $display("%d scoreboard checking",axi_pkt.write_datav_i );   
        if(axi_pkt.write_length_i == 0) begin
		
          this.slave_data_out = axi_pkt.write_data_i;
          #10;
        end
        else
		
          for(int i = 0; i<=axi_pkt.write_length_i ; i++) begin
            $display("axi_pkt.write_length_i = %d",axi_pkt.write_length_i ); 
            this.slave_data_out = axi_pkt.write_data_i;
			
            #10;
            $display("scb...axi_pkt.write_data_i= %h",axi_pkt.write_data_i);
            $display("scb...axi_pkt.read_data_o= %h",axi_pkt.read_data_o);
          end
      end
      if(this.slave_data_out == axi_pkt.read_data_o) begin
        $display("scb...this.slave_data_out= %h",this.slave_data_out);
        $display("scb...axi_pkt.read_data_o= %h",axi_pkt.read_data_o);
        $display(" test passed");
      end
      else
        $display(" test failed");
    end
  endtask : run_phase
endclass : axi_scoreboard