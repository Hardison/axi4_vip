

class axi_seq_item extends uvm_sequence_item;
  
   bit           write_start_i;
   bit [3:0]     write_awid_i;
   bit [31:0]    write_addr_i;
   bit [3:0]     write_length_i;
   bit [2:0]     write_size_i;
   bit [1:0]     write_burst_i;
   bit [1:0]     write_lock_i;
   bit [3:0]     write_wid_i;
   bit [127:0]   write_data_i;
   bit           write_datav_i;
   bit [15:0]    write_strb_i;
   bit           write_ack_o;
  bit                write_data_req_o;
  bit                write_done_o;
  bit                write_err_o;
  bit  [1:0]         write_bresp_o;
  bit  [3:0]         write_bid_o;
  bit                write_bvalid_o;
  
   bit           read_start_i;
   bit [3:0]     read_arid_i;
   bit [31:0]    read_addr_i;
   bit [3:0]     read_length_i;
   bit [2:0]     read_size_i;
       bit [1:0]     read_burst_i;
   bit [1:0]     read_lock_i;
   bit           read_af_i;
  bit                read_ack_o;
  bit [127:0]        read_data_o;
  bit                read_datav_o;
  bit                read_err_o;
  bit                read_done_o;
  
  
  
  
  `uvm_object_utils(axi_seq_item)
  
  function new(string name = "axi_seq_item");
    super.new(name);
  endfunction
  
  

endclass : axi_seq_item
